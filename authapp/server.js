const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose =require('mongoose');
const config = require('./config/config');
const fs = require('fs');
//Connect to database
mongoose.connect(config.database, {useMongoClient: true});
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
//On connection
mongoose.connection.on('connected',()=>{
	console.log('connected to database ' + config.database)
});

//On error
mongoose.connection.on('error',(err)=>{
	console.log('database error ' + err)
});

const app = express();
const users = require('./routes/users');
const devices = require('./routes/devices');
const loginlogs = require('./routes/loginlogs');
const doorlogs = require('./routes/doorlogs');

//port number
const port = config.port;

//cors middleware
app.use(cors());

//Static folder
app.use(express.static(path.join(__dirname,'public')));

//Body Parser Middleware
app.use(bodyParser.json());
app.use('/users',users);
app.use('/devices',devices);
app.use('/loginlogs',loginlogs);
app.use('/doorlogs',doorlogs);

//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

//Server routes
app.get('/',(req,res)=>{
	res.send('Invalid Endpoint');
});

// //reset password
// app.get('/resetpassword',(req,resp)=>{
// 	fs.readFile('./public/resetpassword.html',function(error, data){
// 		if (error) {
// 		  resp.writeHead(404);
// 		  resp.write('Contents you are looking for-not found');
// 		  resp.end();
// 		}  else {
// 		  resp.writeHead(200, {
// 			'Content-Type': 'text/html'
// 		  });
// 		  resp.write(data.toString());
// 		  resp.end();
// 		}
// 	  });
// });

//Start Server
app.listen(port, ()=>{
	console.log('Server starting at port '+ port);
});
// var minutes = 1,
// 	the_interval = minutes * 60 * 1000;

// setInterval(function () {
// 	var timer = new Date();
// 	console.log("I am doing my 1 minutes check at: " + timer);
// 	// do your stuff here
// }, the_interval);


