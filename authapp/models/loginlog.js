const mongoose = require('mongoose');
const nodemailler = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const config = require('../config/config');
const dateFormat = require('dateformat');
/**
 * Create log Schema
 */
const LoginLogSchema = mongoose.Schema({
    Device: {type: String, require: true},
    TypeOfServices: {type: String, require: true},
    DoorStatus: {type: Boolean, require: true},
    Actor:    {type: String, require: true},
    Timestamp:  {type: String, require: true},
    createdAt: { type: Date, expires: config.expiresdoorlog, default: Date.now }
});
const LoginLog = module.exports = mongoose.model('loginlog',LoginLogSchema);

/**
 * add Log
 * @param {*} newLog 
 * @param {*} callback 
 */
module.exports.addLoginLog = function(newLog, callback){
    var day = new Date();
    day = dateFormat(day,"default");
    newLog.Timestamp = day;
    newLog.save(callback);
}

/**
 * Show Log
 * @param {*} callback 
 */
module.exports.traceLog = function(callback){
    LoginLog.find(callback).sort({createdAt: -1});
}

/**
 * List log by Mac Dec Device
 * @param {*} macDevice 
 * @param {*} callback 
 */
module.exports.findByMacDevice = function(macDevice,callback){
    const query = {Device: macDevice}
    LoginLog.findOne(query,callback);
}

/**
 * List log by Services
 * @param {*} service 
 * @param {*} callback 
 */
module.exports.findByServices = function(service,callback){
    const query = {TypeOfServices: service}
    LoginLog.findOne(query,callback);
}

module.exports.findByDate = function(datetime, callback){
    // console.log('datetime: '+datetime);
    var insertDate = dateFormat(datetime,"ddd mmm dd yyyy");
    // console.log('AfterFormat: ',insertDate);
    const query = {Timestamp: new RegExp(insertDate, "m")}
    LoginLog.find(query, callback).sort({createdAt: -1});
}

/**
 * System Log
 * @param {*Log message} msg 
 * @param {*} callback 
 */
// module.exports.system_log = function(msg,callback){
//     var datetime = new Date();
//     var file = 'authapp_system_log.txt';
//     var text = '['+ datetime+'] '+msg+'\r\n';
//     fs.appendFile(file,text,function(err){
//         if(err) throw err;
//     });
// }

/**
 * Add fn send mail while login fail many time
 * @param {*email to login fail} email
 */
module.exports.sendMailLoginFail = function (email, callback) {
    let transporter = nodemailler.createTransport(smtpTransport({
        host: 'mail.adquestasia.com',
        port: 465,
        secureConnection: true,
        auth: {
            user: config.MAILLER_ADQUEST_USERNAME,
            pass: config.MAILLER_ADQUEST_PASSWORD
        },
        secure: true, // use SSL
        tls: {
            rejectUnauthorized: false
        },
    })
    );

    // setup email data with unicode symbols
    let mailOptions = {
        from: config.MAILLER_ADQUEST_USERNAME, // sender address
        to: config.MAILLER_ADQUEST_USERNAME, // list of receivers
        subject: '[QR Adquest Asia] [ Warning Login Notification]', // Subject line
        text: 'Content for Email', // plain text body
        html: '<b>Someone try to access account: </b>' + email + '<br />' +
            '<b> At: </b>' + Date() // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailler.getTestMessageUrl(info));

    });
}
/**
 * 
 */
module.exports.sendEmailRegister = function(email, password, callback){
    let transporter = nodemailler.createTransport(smtpTransport({
        host: 'mail.adquestasia.com',
        port: 465,
        secureConnection: true,
        auth: {
            user: config.MAILLER_ADQUEST_USERNAME,
            pass: config.MAILLER_ADQUEST_PASSWORD
        },
        secure: true, // use SSL
        tls: {
            rejectUnauthorized: false
        },
    })
    );

    // setup email data with unicode symbols
    let mailOptions = {
        from: config.MAILLER_ADQUEST_USERNAME, // sender address
        to: email, // list of receivers
        subject: '[QR Adquest Asia] [Welcome, '+ email + ']', // Subject line
        text: 'Content for Email', // plain text body
        html: 'Hello,  <br /> <br />'
        + ' Use the following values when prompted to log in:  <br />' 
        + ' <b>Email: ' + email + '</b><br />'
        + ' <b>Password: ' + password + '</b><br /><br />'
        + ' If you have any problem, please contact support team! <br /><br />'
        + 'Thanks and Best Regard!,'
        // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailler.getTestMessageUrl(info));

    });
}