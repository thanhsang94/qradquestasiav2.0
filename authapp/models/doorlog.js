const mongoose = require('mongoose');
const config = require('../config/config');
const dateFormat = require('dateformat');
/**
 * Schema Door Status
 */
const DoorLogSchema = mongoose.Schema({
    Device: {type: String, require: true},
    DoorStatus: {type: Boolean, require: true},
    Timestamp:  {type: String, require: true},
    Note:  {type: String, require: true},
    createdAt: { type: Date, expires: config.expiresdoorlog, default: Date.now }
});

const DoorLog = module.exports = mongoose.model('doorlog',DoorLogSchema);
/**
 * Add Log
 * @param {*} newLog 
 * @param {*} callback 
 */
module.exports.addDoorLog = function(doorLog, callback){
    var day = new Date();
    day = dateFormat(day,"default");
    doorLog.Timestamp = day;
    doorLog.save(callback);
}


/**
 * Show Log
 * @param {*} callback 
 */
module.exports.traceLog = function(callback){
    DoorLog.find(callback).sort({createdAt: -1});
}


module.exports.findByDate = function(datetime, callback){
    // console.log('datetime: '+datetime);
    var insertDate = dateFormat(datetime,"ddd mmm dd yyyy");
    // console.log('AfterFormat: ',insertDate);
    const query = {Timestamp: new RegExp(insertDate, "m")}
    DoorLog.find(query, callback).sort({createdAt: -1});
}