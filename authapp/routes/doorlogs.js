const express = require('express');
const router = express.Router();
const DoorLog = require('../models/doorlog');
const passport = require('passport');
const config = require('../config/config');

/**
 * Trace log
 */
router.get('/tracelog', passport.authenticate('jwt', {
    session: false
}), (req, res, next) => {
    DoorLog.traceLog((err, doorlog) => {
        if (err) throw err;
        if (!doorlog) {
            res.json({
                success: false,
                msg: config.ST_Code04
            });
        } else {
            res.send(doorlog);
        }
    });
});


router.post('/tracelogbydate', passport.authenticate('jwt', {
    session: false
}), (req, res, next) => {
    var datetime = req.body.datetime;
    DoorLog.findByDate(datetime, (err, logLog) => {
        if (err) throw err;
        if (!logLog) {
            res.json({
                success: false,
                msg: config.ST_Code04
            });
        } else {
            res.send(logLog);
        }
    });
});
module.exports = router;